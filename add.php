<html>
<head>
    <meta charset="UTF-8">
    <title>Add Data</title>
    <script src="jquery.min.js"></script>  
           <link rel="stylesheet" href="bootstrap.min.css" />  
           <script src="bootstrap.min.js"></script>  
</head>
 
<body>
    
<?php
//including the database connection file
include_once("connection.php");
 
if(isset($_POST['Submit'])) {    
    $name = $_POST['name'];
    $age = $_POST['age'];
    $sex = $_POST['sex'];
    $email = $_POST['email'];
        
    // checking empty fields
    if(empty($name) || empty($age) || empty($sex) || empty($email)) {
                
        if(empty($name)) {
            echo "<font color='red'>Name field is empty.</font><br/>";
        }
        
        if(empty($age)) {
            echo "<font color='red'>Age field is empty.</font><br/>";
        }

        if(empty($sex)) {
            echo "<font color='red'>Sex field is empty.</font><br/>";
        }
        
        if(empty($email)) {
            echo "<font color='red'>Email field is empty.</font><br/>";
        }
        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    } else { 
        // if all the fields are filled (not empty) 
            
        //insert data to database        
        $sql = "INSERT INTO student(name, age, sex, email) VALUES (:name, :age, :sex, :email)";
        $query = $connect->prepare($sql);
                
        $query->bindparam(':name', $name);
        $query->bindparam(':age', $age);
        $query->bindparam(':sex', $sex);
        $query->bindparam(':email', $email);
        $query->execute();

                // Redirect to display result
                header("location: welcome.php");      
    }
}
?>
</body>
</html>