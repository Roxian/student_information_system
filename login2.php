<?php

// Include config file
require_once 'connection.php';
 
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = "<font color='red'>Please enter username.</font>";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST['password']))){
        $password_err = "<font color='red'>Please enter your password.</font>";
    } else{
        $password = trim($_POST['password']);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT username, password FROM user WHERE username = :username";
        
        if($statement = $connect->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $statement->bindParam(':username', $param_username, PDO::PARAM_STR);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if($statement->execute()){
                // Check if username exists, if yes then verify password
                if($statement->rowCount() == 1){
                    if($row = $statement->fetch()){
                        $hashed_password = $row['password'];
                        if(password_verify($password, $hashed_password)){
                            /* Password is correct, so start a new session and
                            save the username to the session */
                            session_start();
                            $_SESSION['username'] = $username;      
                            header("location: welcome.php");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "<font color='red'>Wrong password or invalid.</font>";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "<font color='red'>No account found with that username.</font>";
                }
            } else{
                echo "<font color='red'>Oops! Something went wrong. Please try again later.</font>"; // kung di na maayo inyo relasyon
            }
        }
        
        // Close statement
        unset($statement);
    }
    
    // Close connection
    unset($connect);
}

?>