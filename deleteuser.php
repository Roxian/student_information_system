<?php
//including the database connection file
include("connection.php");
 
//getting id of the data from url
$id = $_GET['id'];
 
//deleting the row from table
$sql = "DELETE FROM user WHERE user_id=:user_id";
$query = $connect->prepare($sql);
$query->execute(array(':user_id' => $id));
 
//redirecting to the display page
header("Location:welcome.php");
?>