<?php
//including the database connection file
include("connection.php");
 
//getting id of the data from url
$id = $_GET['id'];
 
//deleting the row from table
$sql = "DELETE FROM student WHERE stud_id=:stud_id";
$query = $connect->prepare($sql);
$query->execute(array(':stud_id' => $id));
 
//redirecting to the display page
header("Location:welcome.php");
?>