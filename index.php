<?php

// Include config file
require_once 'connection.php';
 
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = "<font color='red'>Please enter username.</font>";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST['password']))){
        $password_err = "<font color='red'>Please enter your password.</font>";
    } else{
        $password = trim($_POST['password']);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT username, password FROM user WHERE username = :username";
        
        if($statement = $connect->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $statement->bindParam(':username', $param_username, PDO::PARAM_STR);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if($statement->execute()){
                // Check if username exists, if yes then verify password
                if($statement->rowCount() == 1){
                    if($row = $statement->fetch()){
                        $hashed_password = $row['password'];
                        if(password_verify($password, $hashed_password)){
                            /* Password is correct, so start a new session and
                            save the username to the session */
                            session_start();
                            $_SESSION['username'] = $username;      
                            header("location: welcome.php");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "<font color='red'>Wrong password or invalid.</font>";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "<font color='red'>No account found with that username.</font>";
                }
            } else{
                echo "<font color='red'>Oops! Something went wrong. Please try again later.</font>"; // kung di na maayo inyo relasyon
            }
        }
        
        // Close statement
        unset($statement);
    }
    
    // Close connection
    unset($connect);
}

?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <script src="jquery.min.js"></script>  
           <link rel="stylesheet" href="bootstrap.min.css" />  
           <script src="bootstrap.min.js"></script>  
</head>
<body>
    <br />  
        <div class="container" style="width:500px;">  
        <h2>Login</h2>
        <p>Please fill in your credentials to login.</p>
        <form action="" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?=isset($username)?$username:'';?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Login">
            </div>

            <div class="form-group">
            <font color='red'><p>Forgot Password?</p></font>
            </div>

            <p>Don't have an account? <a href="signup.php">Sign up now</a>.</p>
        </form>
    </div>
    <br />
</body>
</html>
