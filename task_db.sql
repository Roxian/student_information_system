-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2018 at 03:34 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `stud_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `age` int(3) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`stud_id`, `name`, `age`, `sex`, `email`) VALUES
(9, 'kagura', 18, 'female', 'kagura@gmail.com'),
(10, 'Jonathan N. Cabalquinto', 21, 'male', 'jonathancabalquinto@gmail.com'),
(12, 'Anne C. Batumbakal', 40, 'female', 'annecbatumbakal@gmail.com'),
(13, 'Nobitski', 21, 'male', 'boysupot@xxx.com'),
(15, 'Ronan Keith Alvarez', 19, 'gay', 'ronankeith@gmail.com'),
(19, 'Jennifer Etang', 19, 'female', 'jennyetang@yahoo.com'),
(20, 'Shin Jhianne Murray', 21, 'lesbian', 'shinjhianne@gmail.com'),
(21, 'Mark Devin Denosta', 20, 'male', 'bossmark12@gmail.com'),
(22, 'Pablito Sales Jr.', 20, 'male', 'bosspabs@email.com');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(60) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `email`, `username`, `password`) VALUES
(9, 'Anne Batumbakal', 'Mrs. Batumbakal', 'Mrs. Batumbakal', '$2y$10$ibZALXvFn7PIICHjWsQKg.iHbHF81keN22uniynAfRZ.gtirOg2hy'),
(10, 'Anne C. Batumbakal', 'Anne', 'Anne', '$2y$10$BrnX4S3C6KnXiR94K4pbDeY3NwelmLZeJ6wTeuQnsu0XFvv5Fwa1C'),
(11, 'Pablito Sales Jr.', 'Boss Pabs', 'Boss Pabs', '$2y$10$U7LR6TLJFOymeyXzzOiTFOHt4DTnG1F.3gNUyoEPz8dQq/CLSBUAW'),
(12, 'Roxian Faith', 'Faith', 'Faith', '$2y$10$3hmzslwMadaiaxxqXUTbxOp9cpYFR47deDYZnfySpvHzbfxGxfRAe'),
(13, 'Shinrai Ayase', 'Shinrai', 'Shinrai', '$2y$10$Ahy/DS67aHP1Uaj8Pw0uXu1ftWrlBCFZlUs8Z4p6nHUrBzZIOtdPy'),
(14, 'cute', 'cute', 'cute', '$2y$10$eYNiKFXuTzChVq0bjlIy0OQjCw.J4TGdXJlKZoaGfu57okMZCkJna'),
(15, 'Jennifer Etang', 'Boss Jenn', 'Boss Jenn', '$2y$10$nn59BmAx/TAhybCHPYRzAuLCoyoZvTknW3VSAKAPBAwhQae7np9iC'),
(16, 'ducky momo', 'dukmo', 'dukmo', '$2y$10$RMsF55fteNjVwHUZQUOIR.5iJ8xU5hK34Jk9Z8DtNdzCcr1aneb86'),
(18, 'resbak', 'userka', 'userka', '$2y$10$3i.De9K3syE.Ks4i.K01WerFvpwbVH3NOkxl1/t4uNiQbTGiOFGWG'),
(19, 'mark devin denosta', 'mark', 'mark', '$2y$10$vtFaSrMkJ./rq1ECoO34WOg.bHbkQ3A00bcWZXj.xAq55rNJudS62'),
(20, 'Jonathan', 'Nat', 'Nat', '$2y$10$QWRaK1uz80S9LeGeYA3G9.LSUfa/KXGnQmgtYwGo.gpgcEcFGB192'),
(21, 'Pabs', 'gwapoko', 'gwapoko', '$2y$10$5c6FQqVe4LuLq7LdQM4yVu3vXKYlrmgh6Tztnc6hEznc7LrkyOm5a'),
(22, '', 'dsd', 'dsd', '$2y$10$dd/KLoJLd6M5BbbbmzwR2uXmQxWXdMBHfB9ANHThg6bJzf0GTIeF6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`stud_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `stud_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
