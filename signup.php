
<?php
//Start session
session_start();
// Include connection file
require_once 'connection.php';
 
// Define variables and initialize with empty values
$name = $email = $username = $password = $confirm_password = "";
$name_err = $email_err = $username_err = $password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    //Input name
    if(empty(trim($_POST["name"]))){
        $name_err = "<font color='red'>Please enter a name.</font>";
    } else{
        // Prepare a select statement
        $sql = "SELECT user_id FROM user WHERE name = :name";
        
        if($statement = $connect->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $statement->bindParam(':name', $param_name, PDO::PARAM_STR);
            
            // Set parameters
            $param_name = trim($_POST["name"]);
        }
         
        // Close statement
        unset($statement);
    }

    // Validate email
    if(empty(trim($_POST["email"]))){
        $email_err = "<font color='red'>Please input email.</font>";
    } else{
        // Prepare a select statement
        $sql = "SELECT user_id FROM user WHERE email = :email";
        
        if($statement = $connect->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $statement->bindParam(':email', $email, PDO::PARAM_STR);
            
            // Set parameters
            $param_email = trim($_POST["email"]);
            
            // Attempt to execute the prepared statement
            if($statement->execute()){
                if($statement->rowCount() == 1){
                    $email_err = "<font color='red'>This email is already taken.</font>";
                } else{
                    $email = trim($_POST["email"]);
                }
            } else{
                echo "<font color='red'>Oops! Something went wrong. Please try again later.</font>";
            }
        }
         
        // Close statement
        unset($statement);
    }

    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "<font color='red'>Please enter a username.</font>";
    } else{
        // Prepare a select statement
        $sql = "SELECT user_id FROM user WHERE username = :username";
        
        if($statement = $connect->prepare($sql)){

            // Bind variables to the prepared statement as parameters
            $statement->bindParam(':username', $param_username, PDO::PARAM_STR);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if($statement->execute()){

                if($statement->rowCount() == 1){

                    $username_err = "<font color='red'>This username is already taken.</font>"; // ayaw na lagi panguha ug dili imo
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "<font color='red'>Oops! Something went wrong. Please try again later.</font>"; // kung dili na maayo inyo relasyon
            }
        }
         
        // Close statement
        unset($statement);
    }
    
    // Validate password
    if(empty(trim($_POST['password']))){
        $password_err = "<font color='red'>Please enter a password.</font>";    

    } elseif(strlen(trim($_POST['password'])) < 6){
        $password_err = "<font color='red'>Password must have atleast 6 characters.</font>";
        
    } else{
        $password = trim($_POST['password']);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "<font color='red'>Please confirm password.</font>";     
    } else{
        $confirm_password = trim($_POST['confirm_password']);
        if($password != $confirm_password){
            $confirm_password_err = "<font color='red'>Password did not match.</font>";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($name_err) && empty($email_err) && empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO user (name, email, username, password) VALUES (:name, :email, :username, :password)";
         
        if($statement = $connect->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $statement->bindParam(':name', $param_name, PDO::PARAM_STR);
            $statement->bindParam(':email', $param_username, PDO::PARAM_STR);
            $statement->bindParam(':username', $param_username, PDO::PARAM_STR);
            $statement->bindParam(':password', $param_password, PDO::PARAM_STR);
            
            // Set parameters
            $param_name = $name;
            $param_email = $email;
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash dili mabasa imong password ani besh
            
            // Attempt to execute the prepared statement
            if($statement->execute()){
                // Redirect to login page
                header("location: index.php");
            } else{
                echo "<font color='red'>Something went wrong. Please try again later.</font>";
            }
        }
         
        // Close statement
        unset($statement);
    }
    
    // Close connection
    unset($connect);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <script src="jquery.min.js"></script>  
           <link rel="stylesheet" href="bootstrap.min.css" />  
           <script src="bootstrap.min.js"></script>  
    
</head>
<body> 
        <br />  
        <div class="container" style="width:500px;">  
        <h3>Sign Up</h3>
        <p>Please fill this form to create an account.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                <label>Name</label>
                <input type="text" name="name"class="form-control" value="<?php echo $name; ?>">
                <span class="help-block"><?php echo $name_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                <label>Email</label>
                <input type="text" name="email"class="form-control" value="<?php echo $email; ?>">
                <span class="help-block"><?php echo $email_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="username"class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                <span class="help-block"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>Already have an account? <a href="index.php">Login here</a>.</p>
        </form>
    </div>

</body>
</html>