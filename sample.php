<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE-edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Username</title>

		<link rel="stylesheet" href="bootstrap.min.css">
		<link rel="stylesheet" href="font-awesome.min.css">

		<script src="html5shiv.js"></script>
		<script src="respond.min.js"></script>

	</head>
	<body>
		<div class="container">
			<div class="row profile">
				<div class="col-md-3">
					<div class="profil-sidebar">
						<div class="profile-user-pic">
							<img src="https://guarddome.com/assets/images/profile-img.jpg" alt="" class="imgr-responsive img-circle">
						</div>
						<div class="profile-user-title">
							<div class="profile-user-name">
								Shinrai Ayase
						</div>
						<div class="profile-user-job">
							Web Developer
						</div>
					</div>
					<div class="profile-user-buttons">
						<button class="btn btn-success btn-sm">Follow</button>
						<button class="btn btn-danger btn-sm">Message</button>
					</div>
				</div>

				</div>
			</div>
		</div>
	</body>